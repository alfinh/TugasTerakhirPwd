<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

Class Anggota extends Model
{

  public $table = 't_anggota';

  protected $fillable = ['nik','nama','jk','ttl','alamat'];

}
