<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

Class Buku extends Model
{

  public $table = 't_buku';

  protected $fillable = ['judul','pengarang','penerbit','tahun','jumlah'];

}
