<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

Class Petugas extends Model
{

  public $table = 't_petugas';

  protected $fillable = ['nip','nama_petugas'];

}
