# Tugas Terakhir Pwd

Ini adalah tugas akhir pwd saya berupa Aplikasi System Perpustakaan , merupakan sebuah aplikasi simpel berbasis web yang dibuat menggunakan lumen untuk backend dan 
vue.js untuk frontend. Aplikasi digunakan untuk menyimpan data penting yang menyangkut hal hal tentang perpustakaan.
Seperti : 
1. Anggota
          
2. Buku
          
3. Petugas
          
Untuk menggunakan aplikasi ini anda bisa mengclonenya, link dibawah ini.

[https://gitlab.com/alfinh/TugasTerakhirPwd](https://gitlab.com/alfinh/TugasTerakhirPwd)

Your lumen project must use localhost:8080;

Steps : 


## Build Setup Vue Project

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

# Panduan Menggunakan Aplikasi

## 1. Beranda
Beranda ini digunakan sebagai identitas web dan hal pertama untuk menarik seseorang dalam sebuah web.
![Beranda](pictread/ss1.png)

## 2. Read Data (Fitur Anggota)
Sebuah page yang berisi data data anggota yang didalamnya dapat melakukan sebuah crud.
![ReadData](pictread/ss2.png)

## 3. Create Data (Fitur Anggota)
Sebuah page form untuk melakukan sebuah penambahan data anggota dan lansung bertambah kedatabase.
![ReadData](pictread/ss3.png)

## 4. Edit Data (Fitur Anggota)
Sebuah page form untuk melakukan sebuah Perubahan han data anggota dan lansung berubah didatabasenya pun .
![ReadData](pictread/ss4.png)

Dan ini sedikit penjelasan tentang aplikasi perpustakaan ini semoga membantu dan berguna bagi anda. 
Terima kasih :)
