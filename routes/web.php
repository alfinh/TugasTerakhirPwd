<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//buku
$router->post('/buku','BukuController@create');
$router->get('/buku','BukuController@read');
$router->post('/buku/{id}','BukuController@update');
$router->delete('/buku/{id}','BukuController@delete');
$router->get('buku/{id}','BukuController@detail');

//petugas
$router->post('/petugas','PetugasController@create');
$router->get('/petugas','PetugasController@read');
$router->post('/petugas/{id}','PetugasController@update');
$router->delete('/petugas/{id}','PetugasController@delete');
$router->get('petugas/{id}','PetugasController@detail');

//anggota
$router->post('/anggota','AnggotaController@create');
$router->get('/anggota','AnggotaController@read');
$router->post('/anggota/{id}','AnggotaController@update');
$router->delete('/anggota/{id}','AnggotaController@delete');
$router->get('anggota/{id}','AnggotaController@detail');
