import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Buku from '@/components/Buku'
import BukuForm from '@/components/BukuForm'
import Anggota from '@/components/Anggota'
import AnggotaForm from '@/components/AnggotaForm'
import Petugas from '@/components/Petugas'
import PetugasForm from '@/components/PetugasForm'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/buku',
      name: 'Buku',
      component: Buku
    },
    {
      path: '/buku/create',
      name: 'BukuCreate',
      component: BukuForm
    },
    {
      path: '/buku/:id',
      name: 'BukuEdit',
      component: BukuForm
    },
    {
      path: '/anggota',
      name: 'Anggota',
      component: Anggota
    },
    {
      path: '/anggota/create',
      name: 'AnggotaCreate',
      component: AnggotaForm
    },
    {
      path: '/anggota/:id',
      name: 'AnggotaEdit',
      component: AnggotaForm
    },
    {
      path: '/petugas',
      name: 'Petugas',
      component: Petugas
    },
    {
      path: '/petugas/create',
      name: 'PetugasCreate',
      component: PetugasForm
    },
    {
      path: '/petugas/:id',
      name: 'PetugasEdit',
      component: PetugasForm
    }
  ]
})
